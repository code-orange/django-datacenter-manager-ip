from ipaddress import IPv6Address

from cidrfield.models import IPNetworkField
from macaddress.fields import MACAddressField
from datetime import datetime

from django_datacenter_manager_vlan.django_datacenter_manager_vlan.models import *


class DcIpNetwork(models.Model):
    network = IPNetworkField()
    vlan = models.ForeignKey(DcVlan, models.CASCADE)
    router = models.ForeignKey("DcIpAddress", models.CASCADE, null=True)
    dhcp_enabled = models.BooleanField(default=True)
    dyn_dhcp_enabled = models.BooleanField(default=True)
    dhcp_ipv4_next_server = models.GenericIPAddressField(protocol="IPv4", null=True)
    dhcp_ipv4_boot_file_name = models.CharField(max_length=250, null=True)
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return "Network " + self.network.__str__() + " on " + self.vlan.__str__()

    def save(self, *args, **kwargs):
        # update network date_changed
        self.date_changed = datetime.now()

        # execute save
        super(DcIpNetwork, self).save(*args, **kwargs)

    class Meta:
        db_table = "dc_ip_network"
        unique_together = (("network", "vlan"),)


class DcIpDualStack(models.Model):
    network_ipv4 = models.ForeignKey(DcIpNetwork, models.CASCADE, related_name="+")
    network_ipv6 = models.ForeignKey(DcIpNetwork, models.CASCADE, related_name="+")
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    def save(self, *args, **kwargs):
        # update ip date_changed
        self.date_changed = datetime.now()

        # execute save
        super(DcIpDualStack, self).save(*args, **kwargs)

    class Meta:
        db_table = "dc_ip_dualstack"
        unique_together = (("network_ipv4", "network_ipv6"),)


class DcIpAddress(models.Model):
    ip_address = models.GenericIPAddressField(protocol="both", unpack_ipv4=True)
    mac_address = MACAddressField(default="ff:ff:ff:ff:ff:ff")
    network = models.ForeignKey(DcIpNetwork, models.CASCADE)
    router = models.ForeignKey("self", models.CASCADE, null=True)
    reverse_fqdn = models.CharField(max_length=250, default="myhost.example.com")
    dhcp_enabled = models.BooleanField(default=True)
    dhcp_ipv4_next_server = models.GenericIPAddressField(protocol="IPv4", null=True)
    dhcp_ipv4_boot_file_name = models.CharField(max_length=250, null=True)
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return (
            "IP "
            + self.ip_address.__str__()
            + " in "
            + self.network.__str__()
            + " ("
            + self.reverse_fqdn
            + ")"
        )

    def save(self, *args, **kwargs):
        # update network date_changed
        self.network.date_changed = datetime.now()
        self.network.save()

        # update ip date_changed
        self.date_changed = datetime.now()

        # execute save
        super(DcIpAddress, self).save(*args, **kwargs)

    def recommended_ipv6(self):
        dualstack_networks = DcIpDualStack.objects.filter(
            network_ipv4_id=self.network_id
        )

        for dualstack in dualstack_networks:
            network_ipv6 = dualstack.network_ipv6.network

            if network_ipv6.netmask.__str__() != "ffff:ffff:ffff:ffff::":
                continue

            ipv6_parts = network_ipv6.network_address.__str__().split("/")[0].split(":")
            ipv4_parts = self.ip_address.__str__().split("/")[0].split(".")

            recommended_ipv6_address = IPv6Address(
                "{}:{}:{}:{}:{}:{}:{}:{}".format(
                    ipv6_parts[0],
                    ipv6_parts[1],
                    ipv6_parts[2],
                    ipv6_parts[3],
                    ipv4_parts[0],
                    ipv4_parts[1],
                    ipv4_parts[2],
                    ipv4_parts[3],
                )
            )

            return recommended_ipv6_address.__str__()

        return False

    class Meta:
        db_table = "dc_ip_address"
        unique_together = (("ip_address", "network"),)


class DcIpNatMasquerade(models.Model):
    ip_address = models.ForeignKey(DcIpAddress, models.DO_NOTHING)
    network = models.ForeignKey(DcIpNetwork, models.DO_NOTHING)
    date_added = models.DateTimeField(default=datetime.now)
    date_changed = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return f"{self.ip_address.__str__()} masquerades {self.network.__str__()}"

    def save(self, *args, **kwargs):
        # update network date_changed
        self.network.date_changed = datetime.now()
        self.network.save()

        # update ip date_changed
        self.date_changed = datetime.now()

        # execute save
        super(DcIpNatMasquerade, self).save(*args, **kwargs)

    class Meta:
        db_table = "dc_ip_nat_masquerade"
        unique_together = (("ip_address", "network"),)


class DcIpDhcpOptionType(models.Model):
    name = models.CharField(unique=True, max_length=100)
    description = models.TextField()

    def __str__(self):
        return "DHCP-Option-Type " + self.name

    class Meta:
        db_table = "dc_ip_dhcp_option_type"


class DcIpDhcpOption(models.Model):
    name = models.CharField(unique=True, max_length=100)
    type = models.CharField(max_length=100)
    array = models.BooleanField(default=False)
    configurable = models.BooleanField(default=False)

    def __str__(self):
        return "DHCP-Option " + self.name

    class Meta:
        db_table = "dc_ip_dhcp_option"


class DcIpDhcpOptionGlobalDefault(models.Model):
    option = models.ForeignKey(DcIpDhcpOption, models.DO_NOTHING, unique=True)
    value = models.BinaryField(max_length=200)

    def __str__(self):
        return self.option.__str__()

    class Meta:
        db_table = "dc_ip_dhcp_option_global_default"


class DcIpDhcpGlobalDefault(models.Model):
    dhcp_key = models.CharField(max_length=250, unique=True)
    dhcp_value = models.CharField(max_length=250)

    def __str__(self):
        return self.dhcp_key

    class Meta:
        db_table = "dc_ip_dhcp_global_default"


class DcIpDhcpOptionInstance(models.Model):
    instance = models.ForeignKey(DcInstance, models.DO_NOTHING)
    option = models.ForeignKey(DcIpDhcpOption, models.DO_NOTHING)
    value = models.BinaryField(max_length=200)

    def __str__(self):
        return self.option.__str__() + " for " + self.instance.__str__()

    class Meta:
        db_table = "dc_ip_dhcp_option_instance"
        unique_together = (("instance", "option"),)


class DcIpDhcpOptionIpNetwork(models.Model):
    network = models.ForeignKey(DcIpNetwork, models.DO_NOTHING)
    option = models.ForeignKey(DcIpDhcpOption, models.DO_NOTHING)
    value = models.BinaryField(max_length=200)

    def __str__(self):
        return self.option.__str__() + " for " + self.network.__str__()

    class Meta:
        db_table = "dc_ip_dhcp_option_ip_network"
        unique_together = (("network", "option"),)


class DcIpDhcpOptionIpAddress(models.Model):
    ip_address = models.ForeignKey(DcIpAddress, models.DO_NOTHING)
    option = models.ForeignKey(DcIpDhcpOption, models.DO_NOTHING)
    value = models.BinaryField(max_length=200)

    def __str__(self):
        return self.option.__str__() + " for " + self.ip_address.__str__()

    class Meta:
        db_table = "dc_ip_dhcp_option_ip_address"
        unique_together = (("ip_address", "option"),)


class DcIpDhcpVendorClass(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    dhcp_ipv4_next_server = models.GenericIPAddressField(protocol="IPv4", null=True)
    dhcp_ipv4_boot_file_name = models.CharField(max_length=250, null=True)
    instance = models.ForeignKey(DcInstance, models.DO_NOTHING)

    def __str__(self):
        return self.name + " (" + self.description + ")"

    class Meta:
        db_table = "dc_ip_dhcp_vendor_class"
        unique_together = (("instance", "name"),)


class DcIpDhcpVendorClassOption(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    option_type = models.ForeignKey(DcIpDhcpOptionType, models.DO_NOTHING)
    option_code = models.IntegerField()
    array = models.BooleanField(default=False)
    value = models.BinaryField(null=True)
    code43sub = models.BooleanField(default=True)
    vendor_class = models.ForeignKey(DcIpDhcpVendorClass, models.DO_NOTHING)

    def __str__(self):
        return self.name + " (" + self.description + ")"

    class Meta:
        db_table = "dc_ip_dhcp_vendor_class_option"
        unique_together = (("vendor_class", "option_code"),)


class DcIpDhcpVendorClassMatch(models.Model):
    vendor_class_identifier = models.BinaryField(null=True)
    vendor_class_substring = models.BooleanField(default=False)
    vendor_class_substring_option = models.ForeignKey(
        DcIpDhcpOption, models.DO_NOTHING, null=True
    )
    vendor_class_substring_start = models.IntegerField(default=0)
    vendor_class_substring_end = models.IntegerField(default=0)
    vendor_class_substring_value = models.CharField(max_length=100, null=True)
    vendor_class = models.ForeignKey(DcIpDhcpVendorClass, models.DO_NOTHING)

    def __str__(self):
        return "Match for " + self.vendor_class.name

    class Meta:
        db_table = "dc_ip_dhcp_vendor_class_match"
        unique_together = (("vendor_class_identifier", "vendor_class"),)
