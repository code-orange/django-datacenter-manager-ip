import ipaddress

from django_datacenter_manager_ip.django_datacenter_manager_ip.models import *


def add_network_to_vlan(vlan: DcVlan, network: str):
    ip_network = ipaddress.ip_network(network)

    vlan_ip_network = DcIpNetwork(
        vlan=vlan,
        network=network,
    )

    vlan_ip_network.save(force_insert=True)


def assign_address(
    network: DcIpNetwork, ip_address: str, hw_address: str, reverse_dns: str
):
    ip_address_obj = ipaddress.ip_address(ip_address)

    try:
        ip_address_assignment = DcIpAddress.objects.get(
            network=network, ip_address=str(ip_address_obj)
        )
    except DcIpAddress.DoesNotExist:
        ip_address_assignment = DcIpAddress(
            network=network,
            ip_address=str(ip_address_obj),
        )

    ip_address_assignment.mac_address = hw_address
    ip_address_assignment.reverse_fqdn = reverse_dns

    ip_address_assignment.save()
