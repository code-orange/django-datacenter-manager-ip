from ipaddress import IPv4Network

from celery import shared_task

from django_datacenter_manager_ip.django_datacenter_manager_ip.models import *


@shared_task(name="dc_ip_address_seed")
def dc_ip_address_seed():
    all_subnets = DcIpNetwork.objects.all().order_by("-id")

    for subnet in all_subnets:
        # only seed if ipv4 (ipv6 can be assigned on demand)
        if not isinstance(subnet.network, IPv4Network):
            continue

        for ip in list(subnet.network.hosts()):
            obj, created = DcIpAddress.objects.get_or_create(
                network=subnet,
                ip_address=str(ip),
            )

    return


@shared_task(name="dc_ip_network_router_seed")
def dc_ip_network_router_seed():
    all_subnets_missing_rt = DcIpNetwork.objects.filter(router__isnull=True).order_by(
        "id"
    )

    for subnet in all_subnets_missing_rt:
        try:
            router_ip = subnet.network[1]
        except IndexError:
            router_ip = subnet.network[0]

        obj, created = DcIpAddress.objects.get_or_create(
            network=subnet,
            ip_address=str(router_ip),
        )

        subnet.router = obj
        subnet.save()

    return
