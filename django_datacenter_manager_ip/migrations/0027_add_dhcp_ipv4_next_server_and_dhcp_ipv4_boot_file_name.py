# Generated by Django 3.2.13 on 2022-05-23 13:43

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        (
            "django_datacenter_manager_ip",
            "0026_add_vendor_class_substring_to_dc_ip_dhcp_vendor_class_match",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="dcipaddress",
            name="dhcp_ipv4_boot_file_name",
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AddField(
            model_name="dcipaddress",
            name="dhcp_ipv4_next_server",
            field=models.GenericIPAddressField(null=True, protocol="IPv4"),
        ),
        migrations.AddField(
            model_name="dcipdhcpvendorclass",
            name="dhcp_ipv4_boot_file_name",
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AddField(
            model_name="dcipdhcpvendorclass",
            name="dhcp_ipv4_next_server",
            field=models.GenericIPAddressField(null=True, protocol="IPv4"),
        ),
        migrations.AddField(
            model_name="dcipnetwork",
            name="dhcp_ipv4_boot_file_name",
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AddField(
            model_name="dcipnetwork",
            name="dhcp_ipv4_next_server",
            field=models.GenericIPAddressField(null=True, protocol="IPv4"),
        ),
    ]
