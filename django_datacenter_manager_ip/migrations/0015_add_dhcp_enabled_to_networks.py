# Generated by Django 3.1.13 on 2021-10-04 23:32

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_datacenter_manager_ip", "0014_add_dyn_dhcp_enabled_to_networks"),
    ]

    operations = [
        migrations.AddField(
            model_name="dcipnetwork",
            name="dhcp_enabled",
            field=models.BooleanField(default=True),
        ),
    ]
