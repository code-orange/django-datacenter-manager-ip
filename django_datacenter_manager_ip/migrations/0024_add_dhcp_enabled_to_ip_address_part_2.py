# Generated by Django 3.1.13 on 2021-12-17 00:27

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_datacenter_manager_ip", "0023_add_dhcp_enabled_to_ip_address_part_1"),
    ]

    operations = [
        migrations.AlterField(
            model_name="dcipaddress",
            name="dhcp_enabled",
            field=models.BooleanField(default=True),
        ),
    ]
