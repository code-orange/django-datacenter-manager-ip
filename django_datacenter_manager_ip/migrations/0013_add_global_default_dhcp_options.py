# Generated by Django 3.1.13 on 2021-10-04 22:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        (
            "django_datacenter_manager_ip",
            "0012_configure_unique_together_for_dhcp_options",
        ),
    ]

    operations = [
        migrations.CreateModel(
            name="DcIpDhcpOptionGlobalDefault",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("value", models.BinaryField(max_length=200)),
                (
                    "option",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_datacenter_manager_ip.dcipdhcpoption",
                        unique=True,
                    ),
                ),
            ],
            options={
                "db_table": "dc_ip_dhcp_option_global_default",
            },
        ),
    ]
