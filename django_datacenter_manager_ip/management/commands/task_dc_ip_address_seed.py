from django.core.management.base import BaseCommand

from django_datacenter_manager_ip.django_datacenter_manager_ip.tasks import (
    dc_ip_address_seed,
)


class Command(BaseCommand):
    help = "Run task dc_ip_address_seed"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        dc_ip_address_seed()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
