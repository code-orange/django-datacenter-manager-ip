from django.core.management.base import BaseCommand

from django_datacenter_manager_ip.django_datacenter_manager_ip.models import *


class Command(BaseCommand):
    help = "Run task dc_ip_address_seed"

    def handle(self, *args, **options):
        for network in DcIpNetwork.objects.all():
            print()
            print("NETWORK: " + str(network))

            for ip in network.dcipaddress_set.exclude(mac_address="ff:ff:ff:ff:ff:ff"):
                print("IP " + ip.ip_address + ", Hostname: " + ip.reverse_fqdn)
