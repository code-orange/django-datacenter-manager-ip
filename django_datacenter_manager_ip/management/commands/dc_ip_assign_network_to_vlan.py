from django.core.management.base import BaseCommand

from django_datacenter_manager_ip.django_datacenter_manager_ip.func import (
    add_network_to_vlan,
)
from django_datacenter_manager_vlan.django_datacenter_manager_vlan.models import DcVlan


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("vlan_id", type=int)
        parser.add_argument("network", type=str)

    def handle(self, *args, **options):
        add_network_to_vlan(
            DcVlan.objects.get(id=options["vlan_id"]), options["network"]
        )
