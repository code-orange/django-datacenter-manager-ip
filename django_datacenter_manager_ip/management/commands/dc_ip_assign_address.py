from django.core.management.base import BaseCommand

from django_datacenter_manager_ip.django_datacenter_manager_ip.func import (
    assign_address,
)
from django_datacenter_manager_ip.django_datacenter_manager_ip.models import DcIpNetwork


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("network_id", type=int)
        parser.add_argument("ip_address", type=str)
        parser.add_argument("hw_address", type=str)
        parser.add_argument("reverse_dns", type=str)

    def handle(self, *args, **options):
        assign_address(
            DcIpNetwork.objects.get(id=options["network_id"]),
            options["ip_address"],
            options["hw_address"],
            options["reverse_dns"],
        )
